package com.teamway.app.personalitytest.controller;

import com.teamway.app.personalitytest.model.Question;
import com.teamway.app.personalitytest.model.Quiz;
import com.teamway.app.personalitytest.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QuizController {

    private final QuestionRepository questionRepository;

    @Autowired
    public QuizController(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @PostMapping("/save")
    @Transactional
    public ResponseEntity<Void> save(@RequestBody final Quiz quiz) {
        quiz.getQuestions().forEach(question -> {
            question.getAnswers().forEach(answer -> answer.setQuestion(question));
        });

        questionRepository.saveAll(quiz.getQuestions());
        return ResponseEntity.ok().build();
    }

    @GetMapping("/all")
    public ResponseEntity<Quiz> getAll() {
        final List<Question> questions = questionRepository.findAll();
        final Quiz quiz = new Quiz(questions);

        return ResponseEntity.ok(quiz);
    }
}
