package com.teamway.app.personalitytest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Answer {

    @Id
    @GeneratedValue
    private Long id;

    private String answer;
    private int score;

    @ManyToOne
    @JoinColumn(name = "QUESTION_ID")
    @JsonIgnore
    private Question question;
}
