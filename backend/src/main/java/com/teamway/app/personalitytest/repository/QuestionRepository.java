package com.teamway.app.personalitytest.repository;

import com.teamway.app.personalitytest.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Long> {

}
