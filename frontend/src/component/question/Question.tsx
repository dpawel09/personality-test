import * as React from 'react';
import { connect } from 'react-redux';
import { createSelectAnswer } from '../../store/Action';
import { Question, Store } from '../../store/Store';
import './Question.css';

interface StateProps {
    question: Question;
    totalQuestionNumber: number;
    currentQuestionNumber: number;
}

interface DispatchProps {
    selectAnswer: (score: number) => {},
}

type Props = StateProps & DispatchProps;

const QuestionComponent = (props: Props): JSX.Element => {
    const answers: JSX.Element[] = createAnswerList(props)

    return (
        <div className="question">
            <p>Question {props.currentQuestionNumber} of {props.totalQuestionNumber}</p>

            <p>{props.question?.question}</p>

            <ul>
                {answers}
            </ul>
        </div>
    )
}

const createAnswerList = (props: Props): JSX.Element[] => {
    const result: JSX.Element[] = [];

    if (props.question) {
        props.question.answers.forEach((answer, index) => {
            result.push(
                <li key={index} onClick={() => { props.selectAnswer(answer.score); }}>
                    {answer.answer}
                </li>);
        });
    }

    return result;
}

const mapStateToProps = (store: Store): StateProps => {
    return {
        question: store.quiz.questions[store.quiz.currentQuestionIndex],
        totalQuestionNumber: store.quiz.questions.length,
        currentQuestionNumber: store.quiz.currentQuestionIndex + 1,
    };
};

const mapDispatchToProps = (dispatch: Function): DispatchProps => {
    return {
        selectAnswer: (score: number) => dispatch(createSelectAnswer(score)),
    }
}

const Question = connect(mapStateToProps, mapDispatchToProps)(QuestionComponent);

export {
    Question,
    QuestionComponent,
    mapStateToProps,
    Props
};
