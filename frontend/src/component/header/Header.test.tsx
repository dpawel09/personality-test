import * as React from 'react';
import * as renderer from 'react-test-renderer';
import Header from './Header';

describe('Header', () => {
    test('snapshot', () => {
        // when
        const rendered = renderer.create(<Header />).toJSON();

        // then
        expect(rendered).toMatchSnapshot();
    })
    
});
