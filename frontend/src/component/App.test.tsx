import * as React from 'react';
import * as renderer from 'react-test-renderer/shallow';
import { Store } from '../store/Store';
import { AppComponent, mapStateToProps } from './App';

describe('App', () => {

    test('snapshot', () => {
        // given
        const props = {
            isCompleted: false,
            init: () => {},
        };

        // when
        const rendered = renderer.createRenderer().render(<AppComponent {...props} />);

        // then
        expect(rendered).toMatchSnapshot();
    });

    test('mapStateToProps', () => {
        // given
        const state = {
            quiz: {
                isCompleted: true,
            },
        } as Store;

        // when
        const result = mapStateToProps(state);

        // then
        expect(result.isCompleted).toBe(true);
    });
});
