import * as React from 'react';
import { connect } from 'react-redux';
import { createInit } from '../store/Action';
import { Store } from '../store/Store';
import './App.css';
import Header from './header/Header';
import { Question } from './question/Question';
import { Summary } from './summary/Summary';

interface StateProps {
    isCompleted: boolean;
}

interface DispatchProps {
    init: Function;
}

class AppComponent extends React.Component<StateProps & DispatchProps> {

    componentDidMount() {
        this.props.init();
    }

    render(): JSX.Element {
        return (
            <div className="app">
                <Header />
                {this.props.isCompleted ? <Summary /> : <Question />}
            </div>
        );
    }
}

const mapStateToProps = (state: Store): StateProps => {
    return {
        isCompleted: state.quiz.isCompleted,
    };
}

const mapDispatchToProps = (dispatch: Function): DispatchProps => {
    return {
        init: () => {
            fetch('http://localhost:8080/all')
                .then(response => response.json())
                .then(response => dispatch(createInit(response)));
            
        },
    }
}

const App = connect(mapStateToProps, mapDispatchToProps)(AppComponent);

export {
    AppComponent,
    App,
    mapStateToProps
};
