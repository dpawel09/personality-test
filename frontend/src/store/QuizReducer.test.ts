import quizReducer from "./QuizReducer";
import { Quiz } from "./Store";
import { Action, ActionType } from "./Action";

describe('QuizReducer', () => {

    test('INIT', () => {
        // given
        const state = {} as Quiz;
        const action: Action = {
            type: ActionType.INIT,
            value: {
                questions: [],
            },
        };

        // when
        const result = quizReducer(state, action);

        // then
        expect(result.questions).toBe(action.value.questions);
        expect(result.currentQuestionIndex).toBe(0);
        expect(result.isCompleted).toBe(false);
        expect(result.score).toBe(0);
    });

    test('SELECT_ANSWER', () => {
        // given
        const state = {
            questions: [{}],
            score: 2,
            currentQuestionIndex: 0,
            isCompleted: false,
        } as Quiz;

        const action: Action = {
            type: ActionType.SELECT_ANSWER,
            value: 4,
        };

        // when
        const result = quizReducer(state, action);

        // then
        expect(result.score).toBe(6);
        expect(result.currentQuestionIndex).toBe(1);
        expect(result.isCompleted).toBe(true);
    });

    test('default', () => {
        // given
        const state = {
            questions: [{}],
            score: 2,
            currentQuestionIndex: 0,
            isCompleted: false,
        } as Quiz;

        const action: Action = {
            type: null,
            value: 4,
        };

        // when
        const result = quizReducer(state, action);

        // then
        expect(result.score).toBe(2);
        expect(result.currentQuestionIndex).toBe(0);
        expect(result.isCompleted).toBe(false);
    });
});
