import { Quiz } from "./Store";

interface Action {
    type: ActionType;
    value: any;
}

enum ActionType {
    INIT,
    SELECT_ANSWER,
}

const createInit = (data: Quiz): Action => {
    return {
        type: ActionType.INIT,
        value: data,
    };
};

const createSelectAnswer = (score: number) => {
    return {
        type: ActionType.SELECT_ANSWER,
        value: score,
    };
};

export {
    Action,
    ActionType,
    createInit,
    createSelectAnswer,
}
