import { Quiz } from "./Store";
import { Action, ActionType } from './Action';

const initialState: Quiz = {
    questions: [],
    score: 0,
    currentQuestionIndex: 0,
    isCompleted: false,
};

const quizReducer = (state: Quiz = initialState, action: Action): Quiz => {
    switch (action.type) {
        case ActionType.INIT:
            return {
                questions: action.value.questions,
                currentQuestionIndex: 0,
                score: 0,
                isCompleted: false,
            }
        case ActionType.SELECT_ANSWER:
            const newIndex = state.currentQuestionIndex + 1;

            return {
                ...state,
                score: state.score + action.value,
                currentQuestionIndex: newIndex,
                isCompleted: newIndex >= state.questions.length,
            };
        default:
            return state;
    }
}

export default quizReducer;
